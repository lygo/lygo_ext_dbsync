# LyGo DB Sync 
## Database Synchronization

lygo_db_sync is a client/server TCP/IP database synchronizer 
from local DB to remote DB.

## How to Use

To use just call:

`go get -u bitbucket.org/lygo/lygo_ext_dbsync`

## Dependencies

`go get -u bitbucket.org/lygo/lygo_ext_dbarango`

`go get -u bitbucket.org/lygo/lygo_events`
 
`go get -u bitbucket.org/lygo/lygo_nio`
 
### Versioning

Sources are versioned using git tags:

```
git tag v0.1.0
git push origin v0.1.0
```
 




