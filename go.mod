module bitbucket.org/lygo/lygo_ext_dbsync

go 1.14

require (
	bitbucket.org/lygo/lygo_commons v0.1.4
	bitbucket.org/lygo/lygo_events v0.1.0
	bitbucket.org/lygo/lygo_ext_dbarango v0.1.0
	bitbucket.org/lygo/lygo_ext_logs v0.1.0
	bitbucket.org/lygo/lygo_nio v0.1.0
	github.com/arangodb/go-driver v0.0.0-20200618111046-f3a9751e1cf5
	github.com/pkg/errors v0.8.1
	golang.org/x/sys v0.0.0-20200615200032-f1bc736245b1 // indirect
)
